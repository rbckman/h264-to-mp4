#!/usr/bin/python
import os
import subprocess
import glob
import sys
import re
import time

i = 0

print ""
print "\033[1;34mh264 to mp4 script by rob using ffmpeg"
print "---------------------------------------------------------------"
print "Howto: Place this python script in your Video folder and run it!"
print "\033[1;m"

print "We are in path: ", sys.path[0]
print ""

## searching for files to be muxed
videomuxpath = sys.path[0]

videofiles = glob.glob("%s/*.h264" % (videomuxpath))

if len(videofiles) == 0:
    print "Sorry mate, didn't find any videos"
    print ""
    exit()

print "\033[1;35mVideo files to be converted: ", len(videofiles), "\033[1;m"
print ""

print videofiles

askuser = raw_input('\033[1;41mDo you want to continue? (y)es or (n)o \033[1;m')

if askuser == 'n':
    print "Maybe next time! :)"
    exit()
else:
    print ""
    print "\033[1;44mOkey! Buckle your seatbelt dorothy cause kansas is going bye-bye\033[1;m"
    print ""
    time.sleep(2)
    for video in videofiles:
	lista = ["ffmpeg", "-i", video, "-vcodec", "copy", "%s.mp4"% (video[:-4])]
	subprocess.call(lista)
print ""
print "Done! Thank you and long live free software!"
print ""
